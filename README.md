# apt-verify

## What is apt-verify?

Apt-verify extends [apt](https://salsa.debian.org/apt-team/apt) to
call several tools instead of only GnuPG's `gpgv` for verification of
apt archives.

The motivation for this extension mechanism is to support the
proof-of-concept
[apt-canary](https://gitlab.com/debdistutils/apt-canary) verification
method, and allow others to experiment creating other mechanisms.

## Overview

Once apt-verify is installed, apt will verify downloaded "release"
files by calling out to the [apt-verify-gpgv](apt-verify-gpgv) script
(replacing the call from `apt-key verify` to `gpgv`), which iterate
through commands placed in `/etc/apt/verify.d`, which normally include
the traditional `gpgv` command to perform GnuPG-based verification.

## Installation and dependencies

Put the [apt-verify-gpgv](apt-verify-gpgv) file in the PATH used by
apt, typically `/usr/bin` or `/usr/local/bin`.

The script works with any POSIX-compliant /bin/sh such as dash or GNU
bash, and uses only common tools such as `find`, `sort` and `logger`.

Naturally, `apt` and `gpgv` needs to be installed as well.

Create `/etc/apt/verify.d` and put a symlink to `/usr/bin/gpgv` in
it. See [verify.d/README](verify.d/README) for full documentation on
how the directory is used.

Don't worry if you fail to setup the `gpgv` symlink properly: `apt`
will complain loudly if it cannot find the expected GnuPG verification
output messages, or you have hit a serious bug in apt.

## Usage

Configure `apt` to use the script as a gpgv drop-in.  For example, run
the following as root:

```
echo 'Apt::Key::gpgvcommand "apt-verify-gpgv";' > /etc/apt/apt.conf.d/75verify
```

Test it by running `apt-get update` and you should see something like
the following in `/var/log/syslog`:

```
Feb  8 12:32:08 kaka apt-verify-gpgv: /etc/apt/verify.d/gpgv --homedir /tmp/apt-key-gpghome.i8sOBsTZA5 --keyring /tmp/apt-key-gpghome.i8sOBsTZA5/pubring.gpg --ignore-time-conflict --status-fd 3 /tmp/apt.sig.35yprB /tmp/apt.data.9zW8zG
```

This is how `apt-key verify` called out to `apt-verify-gpgv`.  These
are debug messages, so you may wish to filter them out from the syslog
and they may be disabled by this project eventually.

## License

All files in the apt-verify project are published under the AGPLv3+
see the file [COPYING](COPYING) or
[https://www.gnu.org/licenses/agpl-3.0.en.html].

## Status

I'm currently using apt-verify on the following systems:

- Trisquel aramo on my laptop
- Trisquel nabia on Dell R630 amd64 server
- Debian bullseye on Talos II ppc64el server
- Debian bullseye on amd64/ppc64el VM
- Debian buster on amd64 VM
- Ubuntu focal on amd64 VM

I don't have any Devuan systems running, but plan to test PureOS once
I have time to boot up my old laptop.

## Related work

- https://gitlab.com/debdistutils/apt-canary

## Further work

- Currently syslog is used for outputing debug information.  Is it
  possible to output this to where `apt-get update` is run?  Sending
  it to /dev/tty may work but seems fragile.

- Replacing the `gpgv` tool is a a hack.  The functionality may be
  integrated into apt some other, or via some other existing extension
  mechanism.  One disadvantage with this approach is that it is
  currently not known how to find out which apt repository (apt URL)
  was used to retrieve a particular release file under verification.
  However the advantages with this approach is that it is minimal and
  that it is easy to audit the code.
